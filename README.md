# Yara rule for  APT Group

 this repos is yara rule for apt group trace.

 APT threat actor targeting south korea 
 ## 1. Lazarus (a.k.a Hidden Cobra) 
 lazarus Group is most big hacking group that has been attributed to the north korean government 
 * #### Major Attack  
    [Operation Flame, 1Mission, Troy, DarkSeoul, Ten Days of Rain](https://www.operationblockbuster.com/wp-content/uploads/2016/02/ ) 
 * #### Reference
    [Lazarus Mitre](https://attack.mitre.org/groups/G0032/)

 
 ## 2. Andariel (a.k.a Silent Chollima)
 Andariel is Hacking Group usly attacking wide area, under the Lazarus(Financial sector,ATM, Military Company, Army)
  
  * #### Malware
    BMDoor, BMAgent, Aryan, Ghostrat, Rifdoor, Phandoor, Andaratm
* #### Reference
    [Andariel Group Report by Ahnlab](http://download.ahnlab.com/kr/site/library/%5BReport%5DAndariel_Threat_Group.pdf)\
    [Andariel Group Report by fsec](http://www.fsec.or.kr/common/proc/fsec/bbs/163/fileDownLoad/1752.do)

 ## 3. kimsuky (a.k.a Velvet Chollima)
 Kimsuky Group is most active group in south korea in 2019. They are Attacking Cryptocurrency Exchange user, Political purpose people.
* #### Malware
     DaumRat, NavRat 
* #### Reference
    [Campaign DOKKAEBI by FSEC](http://www.fsec.or.kr/common/proc/fsec/bbs/163/fileDownLoad/1754.do) 

 ## 4. bluenoroff (a.k.a APT 38)
 Bluenoroff is hacking group that mostly attacking finaancial sector under lazarus.

* #### Malware
     Not Yet
* #### Reference
    [APT38 Report By FireEye](https://www.fireeye.kr/content/dam/collateral/kr/apt/rpt-apt38.pdf) \
    [Bluenoroff Report By Kaspersky](https://media.kasperskycontenthub.com/wp-content/uploads/sites/43/2018/03/07180244/Lazarus_Under_The_Hood_PDF_final.pdf) 

 ## 5. ScarCruft (a.k.a APT37, Reaper, Group123, TEMP.Reaper)
 ScarCruft Group, ROKRAT
 * #### Malware
     Not Yet
* #### Reference
    [APT37 Report By FireEye](https://www.fireeye.kr/content/dam/collateral/kr/rpt-apt37.pdf) \
    [Campaign DOKKAEBI by FSEC](http://www.fsec.or.kr/common/proc/fsec/bbs/163/fileDownLoad/1754.do) \
    [ScarCruft Report By Kaspersky](https://media.kasperskycontenthub.com/wp-content/uploads/sites/43/2018/03/07170728/Guerrero-Saade-Raiu-VB2017.pdf) \
    [ReadEye Report By Ahnlab](http://download.ahnlab.com/kr/site/library/[Report]Red_Eyes_Hacking_Group_kor.pdf) 

    